﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuController : MonoBehaviour
{
    
    public static MenuController instance;
    public Animator MainMenuAnim;
    public Animator EndScreenAnim;
    public Animator GameHudAnim;
    public Animator PaddleAnim;

    public GameObject BallFab;
    public GameObject Balls;

    public Button PlayBut;
    public Button RestartBut;

    public bool gameOverBool;

    public int ballCount;
    public int totalHealth;
    public int score;
    public int highScore;
    public TextMeshProUGUI HudLevelTM;
    public TextMeshProUGUI HudScoreTM;
    public TextMeshProUGUI GameOverScoreTM;
    public TextMeshProUGUI GameOverHighScoreTM;
    public TextMeshProUGUI GameOverLevelTM;


    // Start is called before the first frame update
    void Start()
    {
        
        PlayBut.onClick.AddListener(delegate { playButtonFunction(); });
        RestartBut.onClick.AddListener(delegate { restartButtonFunction(); });
        MainMenuAnim.Play("MainMenuEntry");
        AudioController.Instance.PlayMenuBGMusic(AudioController.Instance.BG);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Awake()
    {
        instance = this;
    }


    void playButtonFunction()
    {
        AudioController.Instance.Play(AudioController.Instance.IceBreak);
        ballCount = 1;
        totalHealth = 0;
        score = 0;
        SpawnTiles.Instance.RunningLevel = 1;
        HudScoreTM.text = score.ToString();
        gameOverBool = false;
        //SpawnTiles.Instance.StartGame();
        MainMenuAnim.Play("MainMenuExit");
        GameHudAnim.Play("GameHudEntry");
        PaddleAnim.Play("PaddleEntry");
        StartCoroutine(paddleAnimDisable());
       // startBall();
    }
    
    void restartButtonFunction()
    {
        AudioController.Instance.Play(AudioController.Instance.IceBreak);
        ballCount = 1;
        totalHealth =0;
        score = 0;
        SpawnTiles.Instance.RunningLevel = 1;
        HudScoreTM.text = score.ToString();
        gameOverBool = false;
        //SpawnTiles.Instance.StartGame();
       // MainMenuAnim.Play("MainMenuExit");
        GameHudAnim.Play("GameHudEntry");
        PaddleAnim.Play("PaddleEntry");
        EndScreenAnim.Play("EndScreenExit");
        StartCoroutine(paddleAnimDisable());
        // startBall();
    }


    IEnumerator paddleAnimDisable()
    {
        yield return new WaitForSeconds(.5f);
        PaddleAnim.enabled = false;
        SpawnTiles.Instance.StartGame();
        startBall();

        HudLevelTM.text = SpawnTiles.Instance.RunningLevel.ToString();
    }
    
    void startBall()
    {
        GameObject TmpBall = Instantiate(BallFab, Balls.transform);
        TmpBall.SetActive(true);
    }

    public void gameOver()
    {
        AudioController.Instance.Play(AudioController.Instance.EndIce);
        if (score> PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
        GameOverLevelTM.text = SpawnTiles.Instance.RunningLevel.ToString();
        highScore = PlayerPrefs.GetInt("HighScore",0);
        GameOverHighScoreTM.text = highScore.ToString();
        GameOverScoreTM.text = score.ToString();
        gameOverBool = true;
        SpawnTiles.Instance.destroyAllTiles();
        GameHudAnim.Play("GameHudExit");
        EndScreenAnim.Play("EndScreenEntry");
        PaddleAnim.enabled = true;
        PaddleAnim.Play("PaddleExit");
        for (int i = 1; i < Balls.GetComponentInChildren<Transform>().childCount; i++)
        {
            Destroy(Balls.GetComponentInChildren<Transform>().GetChild(i).gameObject);
        }
    }
    
}
