﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public Rigidbody2D rb;
    public bool inPlay;

    public GameObject BallFab;

    public GameObject Balls;
    

    public ParticleSystem BallHit;
    public GameObject CrackIceFab;
    public Transform CrackIceParent;

    public MenuController MenuCon;



    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = 8 * (rb.velocity.normalized);
        
        //Debug.Log(a.x+" "+a.y);
        //WOn't let the ball move vertically and '8' is the value of force of the ball and more than seven is bad and less than 1 is bad in y
        if (rb.velocity.x < -7 && rb.velocity.y < 1)
        {
            //BallRB.velocity.Set(-7, 1);
            Vector2 newvector = new Vector2(-7, 4);
            rb.velocity = newvector;
            //Debug.Log(BallRB.velocity.x + " " + BallRB.velocity.y);
        }
        else if (rb.velocity.x > 7 && rb.velocity.y < 1)
        {
            //BallRB.velocity.Set(7, 1);
            Vector2 newvector = new Vector2(7, 4);
            rb.velocity = newvector;
            //Debug.Log(BallRB.velocity.x+" "+ BallRB.velocity.y);
        }
        else
        {

        }
    }



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.up * 50);
        
    }

    // Update is called once per frame
    void Update()
    {
        

        //if (!inPlay)
        //{
        //    transform.position = paddle.position;
        //}
    }
    

    private void OnCollisionEnter2D(Collision2D collision)
    {
        BallHit.transform.position = this.gameObject.transform.position;
       // BallHit.Play();
        for(int i =0; i<2; i++)
        {
            GameObject CR = Instantiate(CrackIceFab, BallHit.transform);
            CR.SetActive(true);
            CR.transform.SetParent(CrackIceParent);
        }
        AudioController.Instance.Play(AudioController.Instance.BallTouch);



        if (collision.gameObject.name == "DownScreenEdge")
        {
            Destroy(this.gameObject);
            MenuController.instance.ballCount--;
            if(MenuController.instance.ballCount == 0)
            {
                //GameOver function to be added
                MenuCon.gameOver();
            }
        }
    }



    public void BallPlus()
    {
        MenuController.instance.ballCount++;
        
        GameObject TmpBall = Instantiate(BallFab, Balls.transform);
        TmpBall.SetActive(true);
    }

}
