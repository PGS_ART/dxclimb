﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Vector2 vec = new Vector2(0, 500);
        this.gameObject.GetComponent<Rigidbody2D>().AddForce(vec);
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "UpScreenEdge")
            Destroy(this.gameObject);
        
    }
}
