﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaddleController : MonoBehaviour
{


    public Transform PaddleDragT;
    public GameObject bulletFab;
    public Transform[] bulletpoints;
    public Transform parentOfBullet;

    public Image LargeFill;
    public Image ShootFill;


    public float Hitpoint;

    private float LargePowertime = 0;
    private float shootPowertime = 0;


    private void Update()
    {
        LargeFill.fillAmount = LargePowertime/5*1;
        ShootFill.fillAmount = shootPowertime / 5 * 1;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("Hit1");
        if (collision.gameObject.CompareTag("ball"))
        {
            //Debug.Log("Hit2");
            Hitpoint  = collision.gameObject.transform.position.x - PaddleDragT.position.x;
            //Debug.Log(Hitpoint);
            Vector2 dir = new Vector2(Hitpoint, 1).normalized;

            // Set Velocity with dir * speed

            collision.gameObject.GetComponent<Rigidbody2D>().velocity = dir * 200;
        }


    }


    public void paddleLarge()
    {
        
        if (LargePowertime <= 0)
        {
            Vector3 tmpvec = new Vector3(0.15f, 0.08f, 1);
            this.gameObject.transform.localScale = tmpvec;
            LargePowertime = 5f;
            StartCoroutine(largePowerCor());
            // Debug.Log("Done");
        }
        else
            LargePowertime = 5f;

    }


    IEnumerator largePowerCor()
    {
        LargePowertime = 5f;
        while (LargePowertime > 0f)
        {
            yield return new WaitForSeconds(0.5f);
            LargePowertime -= .5f;
        }
        LargePowertime = 0;
        Vector3 tmpvec = new Vector3(0.1f, 0.08f, 1);
        this.gameObject.transform.localScale = tmpvec;
    }


    public void shootPower()
    {
        if(MenuController.instance.gameOverBool == false)
        StartCoroutine(shootCor());
    }

    IEnumerator shootCor()
    {
        if (shootPowertime <= 0)
        {
            //yield return new WaitForSeconds(.5f);
            shootPowertime = 5f;
            // StartCoroutine(shootPowerCor());
            GameObject item1;
            GameObject item2;
            while (shootPowertime > 0f && MenuController.instance.gameOverBool == false)
            {
                AudioController.Instance.Play(AudioController.Instance.Shoot);
                item1 = Instantiate(bulletFab, bulletpoints[0]);
                item2 = Instantiate(bulletFab, bulletpoints[1]);
                item1.SetActive(true);
                item2.SetActive(true);
                item1.transform.SetParent(parentOfBullet);
                item2.transform.SetParent(parentOfBullet);
                yield return new WaitForSeconds(.5f);
                shootPowertime -= 0.5f;
            }
            shootPowertime = 0;
        }
        else
            shootPowertime = 5f;



    }

    

    private void Start()
    {
        
    }



    private void OnMouseDown()
    {
        PaddleDrag.Instance.MouseDownforChild();
    }
    private void OnMouseDrag()
    {
        PaddleDrag.Instance.MouseDragForChild();
    }

}
