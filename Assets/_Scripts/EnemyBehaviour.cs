﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnemyBehaviour : MonoBehaviour
{
    public Animator GameHudAnim;
    private int LEVEL_UP_ANIM = Animator.StringToHash("LevelUpAnim");
    public Canvas CanvasUI;
    public TextMeshProUGUI PowerUI;
    public TextMeshProUGUI ItemUI;

    public ParticleSystem BallHit;
    public GameObject CrackIceFab;
    public Transform CrackIceParent;

    private int RunningLevel;

    public Transform PowerParent;
    public GameObject Powers;
    private GameObject LargePower;
    private GameObject BallPlussPower;
    private GameObject ShootPower;

    private int itemtype;
    public void SetItemType(int i)
    {
        itemtype = i;
    }


    // Start is called before the first frame update
    [SerializeField]
    int EnemyHealth = 0;
    void Start()
    {
        LargePower = Powers.GetComponentInChildren<Transform>().GetChild(0).gameObject;
        BallPlussPower = Powers.GetComponentInChildren<Transform>().GetChild(1).gameObject;
        ShootPower = Powers.GetComponentInChildren<Transform>().GetChild(2).gameObject;
        //Get All The Powers to be added


        RunningLevel = SpawnTiles.Instance.RunningLevel;
        int TempHealth = Random.Range(RunningLevel, RunningLevel + 2);
        EnemyHealth = TempHealth;
        MenuController.instance.totalHealth += TempHealth;
        ItemUI = Instantiate(PowerUI, CanvasUI.transform);
        ItemUI.transform.position = this.transform.position;
        ItemUI.gameObject.SetActive(true);
        ItemUI.text = EnemyHealth.ToString();
        StartCoroutine(ClimberCor());

        
    }

    //private void Awake()
    //{
    //    Instance = this;
    //}
    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("ball"))
        {
            if (EnemyHealth <= 1)
            {
                ObjectDestroy();


                if (MenuController.instance.totalHealth == 0) // Level End
                {
                    LevelEnd();
                }
            }
            else
            {
                EnemyHealth--;
                MenuController.instance.totalHealth--;
                ItemUI.text = EnemyHealth.ToString();
            }
        }
     
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "DownScreenEdge3")
        {
            MenuController.instance.gameOver();
        }
        if (collision.gameObject.CompareTag("bullet"))
        {
            if (EnemyHealth <= 1)
            {
                ObjectDestroy();


                if (MenuController.instance.totalHealth == 0) // Level End
                {
                    LevelEnd();
                }
            }
            else
            {
                EnemyHealth--;
                MenuController.instance.totalHealth--;
                ItemUI.text = EnemyHealth.ToString();
            }
            Destroy(collision.gameObject);
        }

    }




    IEnumerator ClimberCor()
    {
        while (true)
        {
            yield return new WaitForSeconds(3f);
            
               // Debug.Log("damn");
                Vector3 pos = new Vector3(this.transform.position.x, this.transform.position.y - 0.2f, this.transform.position.z);
            this.transform.position = pos;

            Vector3 pos2 = new Vector3(ItemUI.transform.position.x, ItemUI.transform.position.y - 0.2f, ItemUI.transform.position.z);
            ItemUI.transform.position = pos;



        }


    }




    void LevelEnd()
    {
       // GameHudAnim.Play("LevelUpAnim");
        GameHudAnim.SetTrigger(LEVEL_UP_ANIM);

        Debug.Log("Baal");
        SpawnTiles.Instance.RunningLevel++;
        MenuController.instance.HudLevelTM.text = SpawnTiles.Instance.RunningLevel.ToString();
        SpawnTiles.Instance.StartGame();
        
    }



    void ObjectDestroy()
    {
        AudioController.Instance.Play(AudioController.Instance.IceBreak);
        EnemyHealth--;
        MenuController.instance.totalHealth--;
        MenuController.instance.score++;
        MenuController.instance.HudScoreTM.text = MenuController.instance.score.ToString();
        //Debug.Log(TotalHealth);

        switch (itemtype)
        {
            case 1:
                Powers.transform.position = this.transform.position;
                GameObject PowerItem1 = Instantiate(LargePower, Powers.transform);
                PowerItem1.SetActive(true);
                PowerItem1.transform.SetParent(PowerParent);
                PowerItem1.GetComponent<PowerController>().SetPowerType(0);
                break;
            case 2:
                Powers.transform.position = this.transform.position;
                GameObject PowerItem2 = Instantiate(BallPlussPower, Powers.transform);
                PowerItem2.SetActive(true);
                PowerItem2.transform.SetParent(PowerParent);
                PowerItem2.GetComponent<PowerController>().SetPowerType(1);
                break;
            case 3:
                Powers.transform.position = this.transform.position;
                GameObject PowerItem3 = Instantiate(ShootPower, Powers.transform);
                PowerItem3.SetActive(true);
                PowerItem3.transform.SetParent(PowerParent);
                PowerItem3.GetComponent<PowerController>().SetPowerType(2);
                break;
            default: break;   
            
        }
        for (int i = 0; i < 5; i++)
        {
            GameObject CR = Instantiate(CrackIceFab, this.transform);
            CR.SetActive(true);
            CR.transform.SetParent(CrackIceParent);
        }
        Destroy(this.gameObject);
        Destroy(ItemUI.gameObject);
    }
}
