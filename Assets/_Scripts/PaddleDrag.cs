﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleDrag : MonoBehaviour
{
    public static PaddleDrag Instance;

    private Vector3 mOffset;
    private float mZCoord;
    public Transform T;

    void Awake()
    {
        Instance = this;
    }

    void OnMouseDown()
    {
        mZCoord = Camera.main.WorldToScreenPoint(
        gameObject.transform.position).z;
        mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
        Debug.Log("Down");

    }
    public void MouseDownforChild()
    {
        mZCoord = Camera.main.WorldToScreenPoint(
        gameObject.transform.position).z;
        mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
        //Debug.Log("Down");
    }



    private Vector3 GetMouseAsWorldPoint()

    {
        Vector3 mousePoint =new Vector3 (Input.mousePosition.x,-3.28f,0);
        mousePoint.z = mZCoord;
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    private void Update()
    {
        if (T.position.x < -2)
        {
            T.position = new Vector3(-2f, -3, 0f);
        }
        else if (T.position.x > 2)
        {
            T.position = new Vector3(2f, -3, 0f);
        }
       
    }
    private void Start()
    {
        
        
        
    }

    void OnMouseDrag()

    {
        transform.position = GetMouseAsWorldPoint() + mOffset;
        //Debug.Log("Dragged");
    }
    public void MouseDragForChild()

    {
        transform.position = GetMouseAsWorldPoint() + mOffset;
        //Debug.Log("Dragged");
    }



}
