﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTiles : MonoBehaviour
{
    public static SpawnTiles Instance;

    public int RunningLevel;
    public int StartingTileCount;
    public int TileCount;

    public GameObject SpawnPointsAndItems;
    private GameObject[,] WayPoints = new GameObject[5,6];
    private bool[,] isSpawned = new bool[5, 6];

    public GameObject[] SpawnItems;
    

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        //StartGame();
    }


    public void StartGame()
    {
        
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                isSpawned[i, j] = false;
            }
        }
        //Setting 2d array with Waypoints
        int z = 0;
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                //Debug.Log(i + " " + j + " " + z);
                //Gettting Child Waypints 1 - 30
                WayPoints[i, j] = SpawnPointsAndItems.GetComponentInChildren<Transform>().GetChild(z).gameObject;
                z++;
                //GameObject temp = Instantiate(SpawnItems[1], WayPoints[i, j].transform, false);
                //temp.transform.position = WayPoints[i, j].transform.position;
                //temp.SetActive(true);
            }
        }
        CreateLevel();
    }


    void CreateLevel()
    {
        TileGenaration(RunningLevel);
    }

    void TileGenaration(int RunningLevel)
    {
        TileCount = StartingTileCount;
        for(int i = 1; i < RunningLevel; i+=2)
        {
            TileCount += 1;
        }
        if (TileCount > 25) TileCount = 25;

        // Code to be Added
        int xArrayRand;
        if (TileCount > 23)
        {
            xArrayRand = 5;
        }
        else if (TileCount > 17)
        {
            xArrayRand = 4;
        }
        else if(TileCount > 11)
        {
            xArrayRand = 3;
        }
        else if(TileCount > 5)
        {
            xArrayRand = 2;
        }
        else
        {
            xArrayRand = 1;
        }

        int itemnumber;
        int probability;
        for (int i = 0; i < TileCount; i++)
        {
            
            probability = Random.Range(0, 100);
            if (-1 < probability && probability < 50)
                itemnumber = 0;
            else if (49 < probability && probability < 75)
                itemnumber = 1;
            else if (75 < probability && probability < 90)
                itemnumber = 2;
            else
                itemnumber = 3;

            bool spawned = true;
            do{
                    int Xarray = Random.Range(0, xArrayRand);
                    int Yarray = Random.Range(0, 6);
                    if (isSpawned[Xarray, Yarray] == false)
                    {
                        isSpawned[Xarray, Yarray] = true;
                        spawned = false;
                    //Debug.Log(Xarray + " " + Yarray);
                        GameObject TempItem = Instantiate(SpawnItems[itemnumber], WayPoints[Xarray, Yarray].transform);
                        TempItem.transform.position = WayPoints[Xarray, Yarray].transform.position;
                        TempItem.SetActive(true);
                        TempItem.GetComponent<EnemyBehaviour>().SetItemType(itemnumber);
                    
                    }
            }while(spawned);
        }
        
    }

    public void destroyAllTiles()
    {
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if(WayPoints[i, j].GetComponentInChildren<Transform>().childCount == 1)
                {
                    Destroy( WayPoints[i,j].GetComponentInChildren<Transform>().GetChild(0).gameObject);
                    Destroy(WayPoints[i, j].GetComponentInChildren<Transform>().GetChild(0).GetComponent<EnemyBehaviour>().ItemUI);
                }
            }
        }
    }
    
}
