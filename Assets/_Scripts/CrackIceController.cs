﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrackIceController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Vector2 x = new Vector2(Random.Range(-50f, 50f), Random.Range(-50f, 50f));
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(x);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "DownScreenEdge")
        Destroy(this.gameObject);
    }
}
