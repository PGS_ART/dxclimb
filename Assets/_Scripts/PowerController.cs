﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerController : MonoBehaviour
{

    private int PowerType;
    public PaddleController padCon;
    public void SetPowerType(int i)
    {
        PowerType = i;
    }

    public GameObject Ball;
    
    

    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Paddle" && MenuController.instance.gameOverBool == false)
        {
            AudioController.Instance.Play(AudioController.Instance.Consume);
            if (PowerType == 1)
            {
                Ball.GetComponent<BallController>().BallPlus();
            }
            else if (PowerType == 0)
            {
                collision.gameObject.GetComponent<PaddleController>().paddleLarge();
            }
            else if (PowerType == 2)
            {
                padCon.shootPower();
            }
            Destroy(this.gameObject);
        }else if(collision.gameObject.name == "PaddDownScreenEdge2")
        {
            Destroy(this.gameObject);
        }
        
    }
    

   
}
