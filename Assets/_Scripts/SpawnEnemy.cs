﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    public List<GameObject> totalSpawningPoint;
    public List<GameObject> totalSpawningItem;
    public Transform containter;


    List<int> occupiedPoint =  new List<int>() ;
        List<int> row = new List<int>();
        List<int> column  = new List<int>();
        List<int>  angle = new List<int>();
    int tempEnemySpawnNumber = 0;
    public int m_runningLevelNumber;
    // Start is called before the first frame update
    void Start()
    {
        m_runningLevelNumber = 10;

        EnemySpawnningPoint();
        row.Add(1);
        row.Add(2);
        row.Add(3);
        row.Add(4);
        row.Add(5);

        column.Add(6);
        column.Add(12);
        column.Add(18);

        angle.Add(7);
        angle.Add(14);
        angle.Add(21);
    }

    // Update is called once per frame
    

    int EnemySpawninTheCurrentLevel()
    {
        //Debug.Log("Inside Enemy Spawn");
        if(m_runningLevelNumber%2 != 0)
        {
            //Debug.Log("Inside %2");

            tempEnemySpawnNumber = m_runningLevelNumber + 2;
            if (tempEnemySpawnNumber > 20)
            {
                tempEnemySpawnNumber = 20;
            }
            //Debug.Log("Enemy Spawn Number  "+ tempEnemySpawnNumber);
            return tempEnemySpawnNumber;
        }
        else
        {
            //Debug.Log("Inside else %2");
            return tempEnemySpawnNumber;
        }
    }
    void EnemySpawnningPoint()
    {
        int temp_EnemySpawninTheCurrentLevel = EnemySpawninTheCurrentLevel();
        int temp_totalSpawningPoint = totalSpawningPoint.Count;
        int temp_totalSpawningItem = totalSpawningItem.Count;

        if (temp_EnemySpawninTheCurrentLevel <= 2)
        {
            int enemyGrowPoint = Random.Range(1, (temp_totalSpawningPoint - 6));

        }
        else
        {
            while(occupiedPoint.Count != temp_EnemySpawninTheCurrentLevel)
            {
                int tempRen = Random.Range(1, temp_totalSpawningPoint);
                bool tempBool = true;

                for (int i = 0; i < occupiedPoint.Count; i++)
                {
                    if (occupiedPoint[i] == tempRen)
                    {
                        tempBool = false;
                        break;
                    }
                }
                if (tempBool)
                {
                    occupiedPoint.Add(tempRen);
                }               
            }
            
            for (int i = 0; i < occupiedPoint.Count; i++)
            {
                //Debug.Log("Occupied point Value " + occupiedPoint[i]);

                int spwanItemTemp = Random.Range(1, temp_totalSpawningItem);
                if (spwanItemTemp == 1 || spwanItemTemp == temp_totalSpawningItem)
                {
                    spwanItemTemp = spwanItemTemp - 1;
                }


                GameObject tempInst = Instantiate(totalSpawningItem[spwanItemTemp], containter,false);
                tempInst.transform.position = totalSpawningPoint[(occupiedPoint[i])-1].transform.position;
            }
            
        }
    }
}
